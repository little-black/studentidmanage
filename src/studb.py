import sqlite3

conn = 0
c = 0
def conn2db():
    global conn
    global c
    conn = sqlite3.connect('studentInfo.db')
    print('connect to sqlite3 successfully!')
    c = conn.cursor()

def insert(stu):
    global conn
    global c
    cmd = "INSERT INTO student (ID,NAME,AGE,SEX,GRADE,PHOTO) \
        VALUES (%d,\"%s\",%d,\"%s\",%d,\"%s\");"%(int(stu.id),stu.name,int(stu.age),stu.sex,int(stu.grade),stu.photo)
    print(cmd)
    try:
        c.execute(cmd)
    except :
        print("=========================================")
        print("Error maybe ID has been exiting")
        print(stu.id)
        print("=========================================")
        return False
    conn.commit()
    return True

def find(id):
    global conn
    global c
    cmd = "SELECT * FROM student where id=%d"%id
    print(cmd)
    data = c.execute(cmd)
    # for x in data:
    #     print(x)
    print("find successfully")
    return data

def clearAll():
    global conn
    global c
    cmd = "DELETE FROM student"
    c.execute(cmd)
    print("clean all data")

def findAll():
    global conn
    global c
    cmd = "SELECT * FROM student"
    alldata = c.execute(cmd)
    return alldata
