# -*- coding:utf-8 -*-

import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog, QGraphicsScene, QGraphicsView
from PyQt5.QtGui import QPixmap
from gui.gui5 import *
from student import *
from studb import *
from word import *
import docx
# import os
imgpath = None
class myUI(QMainWindow, Ui_MainWindow):
    # for  init UI and connect signal and slot
    def __init__(self):
        super(QMainWindow, self).__init__()
        self.setupUi(self)
        self.myConnect()
        conn2db()           # connect to student.db
        self.cmd.append("welcome to program!")

    # connect signal and slot
    def myConnect(self):
        self.input_btn.clicked.connect(self.inputStuInfo)
        self.find_btn.clicked.connect(self.findStuByid)
        self.open_menu.triggered.connect(self.readStuInfo)
        self.clear_menu.triggered.connect(self.clearTxt)
        self.output_menu.triggered.connect(self.output2txt)
        self.chosePhoto_btn.clicked.connect(self.chosePhoto)
        # image = QPixmap()
        # print(image.load("timg.jpg"))
        # self.photo.setPixmap(image.scaled(self.photo.width(),self.photo.height()))
        # self.photo.setPixmap(image)
        # print(image.load())
        # .close()

    # output student info to docx
    def output2txt(self):
        opendocx()
        data = findAll()
        # file = open("studentInfo.txt",'w')
        # for x in data:
        #     print(x,file=file)
        # self.cmd.append("outpout to file successfully!")
        for x in data:
            insert2docx(x)
        closedocx()
        self.cmd.append("output to studentInfo.docx successfully!")

    # clear all log, but i dont use it
    def clearTxt(self):
        clearAll()
        file = open("student_information.txt", 'w')
        file.write('')
        self.cmd.append("clear all data!")

    # choose a photo for a student
    def chosePhoto(self):
        global imgpath
        try:
            photoname = QFileDialog.getOpenFileName(self, "CHOOSE FILE", "./")
            img = QPixmap()
            imgpath = photoname[0]
            img.load(photoname[0])
        except:
            print("Error!")
            self.cmd.append("Error operator!")
        else:
            self.photo.setPixmap(img.scaled(self.photo.width(),self.photo.height()))

    # read student info from a txt file
    def readStuInfo(self):
        # open a file
        try:
            filename = QFileDialog.getOpenFileName(self, "CHOOSE FILE", "./")
            file = open(filename[0], 'r', encoding="utf-8")
        except:
            print("Error!")
            self.cmd.append("Error operator!")
        else:
            # database = open("student_information.txt", 'r')
            #info = []
            while True:
                try:
                    data = file.readline()
                except:
                    self.cmd.append("Error input file")
                else:
                    print(data)
                    if data == '':
                        break
                    # data = data.encode('gbk').decode('utf-8')
                    #info.append(data.split('_'))
                    i = data.split('_')
                    try:
                        print(i[6])
                    except:
                        self.cmd.append("Error input file")
                        return
                    else:
                        stu = student(i[0],i[1],i[2],i[3],i[4],i[5])
                        if insert(stu):
                            self.cmd.append("%d insert to database successfully"%int(stu.id))
                        else:
                            self.cmd.append("%d insert to database faild"%int(stu.id))
                        print(data)

            # data = file.read()
            # data = data.encode('gbk')
            # data = data.decode('utf-8')
            # print(info)
            # file = open("student_information.txt", 'a')
            # for i in info:
            #     stu = student(i[0],i[1],i[2],i[3],i[4])
            #     insert(stu)
            #     data = stu.id + '_' +stu.name + '_' + stu.age + '_' + stu.sex + '_' + stu.grade
            #     data = data.encode('utf-8').decode('gbk')
            #     print(data,file=file, end = '')

            file.close()
            print("read complete")
            self.cmd.append("read from file copmpletely!")

    # find a student info by id
    def findStuByid(self):
        try:
            id = int(self.id_input.text())
        except:
            self.cmd.append("please input true ID")
        else:
            print("%d is you want to  find"%id)
            people = find(id)
            data = people.fetchone()
            print(data)
            if data == None:
                self.cmd.append("no this ID")
            else:
                stu = student(data[0],data[1],data[2],data[3],data[4],data[5])
                self.name_input.setText(stu.name)
                self.age_input.setText(str(stu.age))
                self.sex_input.setText(stu.sex)
                self.grade_input.setText(str(stu.grade))
                self.cmd.append("find successfully!")
                self.photo.setPixmap(QPixmap(stu.photo).scaled(self.photo.width(),self.photo.height()))

    # insert a student info to database
    def inputStuInfo(self):
        global imgpath
        info = [
            self.id_input.text(),
            self.name_input.text(),
            self.age_input.text(),
            self.sex_input.text(),
            self.grade_input.text(),
            imgpath
        ]
        # if a property is null then qual '0'
        for i in range(0, len(info)):
            if info[i] == '':
                info[i] = '0'
        if info[-1] == '0':
            info[-1] = None
        stu = student(info[0],info[1],info[2],info[3],info[4],info[5])
        self.prn_obj(stu)
        if insert(stu):
            self.cmd.append("input successfully!")
        else:
            self.cmd.append("input faild ID has been exiting")
        #data = stu.id + '_' +stu.name + '_' + stu.age + '_' + stu.sex + '_' + \
        #    stu.grade
        file = open("student_information.txt", 'a')
        data = info[0] + ' ' + info[1] + ' ' + info[2] + ' ' + info[3] + ' ' + \
                 info[4]+ ' ' + str(info[5])
        print(data,file=file)
        # data = data.encode('utf-8').decode('gbk')
        #data = data.encode('utf-8').decode('gbk')
        #data = data.encode('utf-8')
        #data = data.decode('gbk')
        #print(data)
        #print(data,file=file)
        #file.close()
        #self.cmd.append("input successfully\n")


    #this func for print a class all property
    def prn_obj(self, obj,_file=sys.stdout):
        print ('\n'.join(['%s:%s' % item for item in obj.__dict__.items()]),file=_file)
