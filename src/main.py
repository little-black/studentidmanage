# -*- coding: utf-8 -*-
import sys
from PyQt5.QtWidgets import QApplication, QMainWindow
from gui import *
from myInit import *

if __name__ == '__main__':
    print("program is running")
    app = QApplication(sys.argv)        # new app
    # mainWindow = QMainWindow()          # new windows
    # ui = Ui_MainWindow()                # a class of ui
    # ui.setupUi(mainWindow)              # setup ui
    # mainWindow.show()                   # load ui to windows

    mainWindow = myUI()
    mainWindow.show()

    sys.exit(app.exec_())
